package ru.kev.fraction;


/**
 * Класс для демонстрации работоспособности класса Parser
 *
 * @author Kotelnikova E.V. group 15oit20
 */
public class Main {

    public static void main(String[] args) {
        System.out.println("Введите выражение: ");
        String[] expression = Parser.input();
        System.out.println(expression[0] + " " + expression[1] + " " + expression[2] + " = " + Parser.calc(expression));


    }
}
