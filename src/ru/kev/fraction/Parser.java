package ru.kev.fraction;

import java.util.Scanner;
import java.util.regex.*;

/**
 * Этот класс для ввода выражения строкового типа, разделения выражения на части и для арифмических вычислений
 *
 * @author Kotelnikova E.V. group 15oit20
 */
public class Parser {
    static Scanner scanner = new Scanner(System.in);

    public static String[] input() {
        String expression;
        boolean value;
        do {
            expression = scanner.nextLine();
            value = checkup(expression);
            if (value == false) {
                System.out.println("Данные введены некорректно, повторите ввод:");
            }
        } while (value == false);


        String[] splitLine = new String[3];
        int i = 0;
        for (String string : expression.split(" ")) {
            splitLine[i] = string;
            i++;
        }
        return splitLine;

    }

    public static Rational calc(String[] splitLine) {
        Rational result = new Rational();
        Rational rational1 = splitArray(splitLine[0]);
        Rational rational2 = splitArray(splitLine[2]);
        String statement = splitLine[1];

        switch (statement) {
            case "+":
                result = rational1.add(rational2);
                break;
            case "-":
                result = rational1.sub(rational2);
                break;
            case "/":
                result = rational1.div(rational2);
                break;
            case "*":
                result = rational1.mult(rational2);
                break;
        }

        return result;
    }

    private static boolean checkup(String expression) {
        Pattern pattern = Pattern.compile("[1-9]+\\d*[/][1-9]+\\d*\\s[+]*[-]*[*]*[/]*\\s[1-9]+\\d*[/][1-9]+\\d*");
        Matcher matcher = pattern.matcher(expression);
        return matcher.find();//TODO TRY return matcher.matches()
    }


    private static Rational splitArray(String splitLine) {
        String[] fraction = new String[2];
        int i = 0;
        for (String  number: splitLine.split("/")) {
            fraction[i] = number;
            i++;
        }
        return new Rational(Integer.parseInt(fraction[0]),Integer.parseInt(fraction[1]));
    }


}



