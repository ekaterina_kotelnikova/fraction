package ru.kev.fraction;

/**
 * Класс содержит описание дроби и реализацию арифметических вычислений
 *
 * @author Kotelnikova E.V. group 15oit20
 */
public class Rational {
    private int numerator;
    private int denominator;

    public Rational(int numerator, int denominator) {
        this.numerator = numerator;
        if (denominator == 0) {
            throw new ArithmeticException("Знаменатель не может быть равен нулю!");
        }
        this.denominator = denominator;
    }

    public Rational() {
        this(1, 1);
    }

    public int getNumerator() {
        return numerator;
    }

    public int getDenominator() {
        return denominator;
    }

    public void setNumerator(int numerator) {
        if (denominator == 0) {
            throw new ArithmeticException("Знаменатель не может быть равен нулю!");
        }
        this.numerator = numerator;
    }

    public void setDenominator(int denominator) {
        this.denominator = denominator;
    }

    @Override
    public String toString() {
        if (denominator == 1) {
            return numerator + "";
        } else {
            return numerator + "/" + denominator;
        }
    }

    public Rational mult(Rational fraction) {
        Rational result = new Rational(this.numerator * fraction.numerator, this.denominator * fraction.denominator);
        reduce(result);
        return result;
    }

    public Rational div(Rational fraction) {
        Rational result = new Rational(this.numerator * fraction.denominator, this.denominator * fraction.numerator);
        reduce(result);
        return result;
    }

    public Rational sub(Rational fraction) {
        Rational result = new Rational();
        if (this.denominator > fraction.denominator && this.denominator % fraction.denominator == 0) {
            fraction.numerator = (this.denominator / fraction.denominator) * fraction.numerator;
            result = new Rational(this.numerator - fraction.numerator, this.denominator);
            reduce(result);
            return result;
        }

        if (this.denominator < fraction.denominator && fraction.denominator % this.denominator == 0) {
            this.numerator = (fraction.denominator / this.denominator) * this.numerator;
            result = new Rational(this.numerator - fraction.numerator, fraction.denominator);
            reduce(result);
            return result;
        }

        if (this.denominator == fraction.denominator) {
            result = new Rational(this.numerator - fraction.numerator, this.denominator);
            reduce(result);
            return result;
        }

        if (fraction.denominator % this.denominator != 0 || this.denominator % fraction.denominator != 0) {
            this.numerator = fraction.denominator * this.numerator;
            fraction.numerator = this.denominator * fraction.numerator;
            result = new Rational(this.numerator - fraction.numerator, this.denominator * fraction.denominator);
            reduce(result);
            return result;
        }
        return result;

    }

    public Rational add(Rational fraction) {
        Rational result = new Rational();
        if (this.denominator > fraction.denominator && this.denominator % fraction.denominator == 0) {
            fraction.numerator = (this.denominator / fraction.denominator) * fraction.numerator;
            result = new Rational(this.numerator + fraction.numerator, this.denominator);
            reduce(result);
            return result;
        }

        if (this.denominator < fraction.denominator && fraction.denominator % this.denominator == 0) {
            this.numerator = (fraction.denominator / this.denominator) * this.numerator;
            result = new Rational(this.numerator + fraction.numerator, fraction.denominator);
            reduce(result);
            return result;
        }

        if (this.denominator == fraction.denominator) {
            result = new Rational(this.numerator + fraction.numerator, this.denominator);
            reduce(result);
            return result;
        }

        if (fraction.denominator % this.denominator != 0 || this.denominator % fraction.denominator != 0) {
            this.numerator = fraction.denominator * this.numerator;
            fraction.numerator = this.denominator * fraction.numerator;
            result = new Rational(this.numerator + fraction.numerator, this.denominator * fraction.denominator);
            reduce(result);
            return result;
        }
        return result;
    }

    public static Rational reduce(Rational fraction) {
        for (int i = fraction.denominator; i >= 1; i--) {
            if (fraction.numerator % i == 0 & fraction.denominator % i == 0) {
                fraction.numerator /= i;
                fraction.denominator /= i;
            }
        }
        return fraction;
    }
}
